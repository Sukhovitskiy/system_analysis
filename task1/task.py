import csv
import sys


def print_function(qqq, x, y):
    for r in enumerate(qqq):
        if r[0] == x:
            print(r[1][y])


def read(path, x, y):
    with open(path, encoding='utf-8') as file:
        print_function(csv.reader(file, delimiter=","), x, y)


if __name__ == '__main__':
    read(sys.argv[1], int(sys.argv[2]), int(sys.argv[3]))
